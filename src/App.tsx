import React from "react";
import logo from "./logo.svg";
import "./App.css";

import { JokeCard } from "./components/JokeCard";

import * as chuckNorrisApi from "./service/chucknorris";

function App() {
  const [randomJoke, setRandomJoke] = React.useState();

  React.useEffect(() => {
    const fetchRandomJoke = async () => {
      const response = await chuckNorrisApi.getRandomChuckJoke();

      console.log(response);
      setRandomJoke(response);
    };

    fetchRandomJoke();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        {randomJoke && (
          <div>
            <JokeCard data={randomJoke} />
          </div>
        )}
      </header>
    </div>
  );
}

export default App;
