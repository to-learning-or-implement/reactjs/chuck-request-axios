import { api } from "./api";

import { GetRandomJokeFromCategory } from "./dto/GetRandomJokeFromCategory";
import { SearchFreeText } from "./dto/SearchFreeText";

export const getRandomChuckJoke = async () => {
  const res = await api.get("random");

  return res.data;
};

export const getRandomJokeFromCategory = async (
  params: GetRandomJokeFromCategory
) => {
  const res = await api.get(`random?category=${params.category}`);

  return res.data;
};

export const getAvailableCategories = async () => {
  const res = await api.get("categories");

  return res.data;
};

export const searchFreeText = async (params: SearchFreeText) => {
  const res = await api.get(`search?query=${params.query}`);

  return res.data;
};
