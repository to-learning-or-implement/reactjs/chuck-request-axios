enum JokeCategory {
  "animal",
  "career",
  "celebrity",
  "dev",
  "explicit",
  "fashion",
  "food",
  "history",
  "money",
  "movie",
  "music",
  "political",
  "religion",
  "science",
  "sport",
  "travel",
}

export interface GetRandomJokeFromCategory {
  category: JokeCategory;
}
