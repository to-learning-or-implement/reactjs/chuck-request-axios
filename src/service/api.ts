import axios from "axios";

const api = axios.create({
  baseURL: "https://api.chucknorris.io/jokes",
  headers: { "X-Custom-Header": "chucknorris" },
});

export { api };
