import { IJoke } from "./joke";

export interface ISearchResponse {
  total: number;
  result: IJoke[];
}
