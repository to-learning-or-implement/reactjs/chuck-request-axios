import React from "react";

import { IJoke } from "../../models/joke";

interface JokeCardProps {
  data: IJoke;
}

export const JokeCard = ({ data }: JokeCardProps) => {
  return (
    <div>
      <h4>{data.value}</h4>
      <p>Created at: {data.created_at.split(" ")[0]}</p>
    </div>
  );
};
